package com.tsc.smironova.tm.controller;

import com.tsc.smironova.tm.api.controller.ICommandController;
import com.tsc.smironova.tm.api.service.ICommandService;
import com.tsc.smironova.tm.model.Command;
import com.tsc.smironova.tm.util.ColorUtil;
import com.tsc.smironova.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showIncorrectArgument() {
        System.err.println("Error! Argument was not found!");
    }

    @Override
    public void showIncorrectCommand() {
        System.err.println("Error! Command was not found! Enter command help.");
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Svetlana Mironova");
        System.out.println("E-MAIL: smironova@tsconsulting.com");
        System.out.println("--------------------");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
        System.out.println("--------------------");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands)
            System.out.println(command.toString());
        System.out.println("--------------------");
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands)
            System.out.println(command.getName());
        System.out.println("--------------------");
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String arg = command.getArgument();
            if (arg != null)
                System.out.println(arg);
        }
        System.out.println("--------------------");
    }

    @Override
    public void showInfo() {
        final int processors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("[SYSTEM INFO]");
        System.out.println("Available processors: " + processors);
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
        System.out.println("--------------------");
    }

    @Override
    public void exit() {
        System.out.println(ColorUtil.PURPLE + "[EXIT]" + ColorUtil.RESET);
        System.exit(0);
    }

}
