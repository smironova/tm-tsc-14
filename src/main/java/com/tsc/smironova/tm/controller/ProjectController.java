package com.tsc.smironova.tm.controller;

import com.tsc.smironova.tm.api.controller.IProjectController;
import com.tsc.smironova.tm.api.service.IProjectService;
import com.tsc.smironova.tm.enumerated.Sort;
import com.tsc.smironova.tm.enumerated.Status;
import com.tsc.smironova.tm.model.Project;
import com.tsc.smironova.tm.util.ColorUtil;
import com.tsc.smironova.tm.util.SystemOutUtil;
import com.tsc.smironova.tm.util.TerminalUtil;
import com.tsc.smironova.tm.util.ValidationUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjectList() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(ColorUtil.PURPLE + Arrays.toString(Sort.values()) + ColorUtil.RESET);
        final String sort = TerminalUtil.nextLine();
        final List<Project> projects;
        if (ValidationUtil.isEmpty(sort))
            projects = projectService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(ColorUtil.PURPLE + sortType.getDisplayName() + ColorUtil.RESET);
            projects = projectService.findAll(sortType.getComparator());
        }
        if (ValidationUtil.isEmpty(projects)) {
            System.out.println(ColorUtil.RED + "No projects! Add new project." + ColorUtil.RESET);
            SystemOutUtil.printFailMessage();
            return;
        }
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.add(name, description);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void showProject(final Project project) {
        if (project == null)
            return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        showProject(project);
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        showProject(project);
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void showProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        showProject(project);
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void removeProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(id);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        showProject(project);
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeOneByIndex(index);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        showProject(project);
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(name);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        showProject(project);
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdatedId = projectService.updateProjectById(id, name, description);
        if (projectUpdatedId == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdatedIndex = projectService.updateProjectByIndex(index, name, description);
        if (projectUpdatedIndex == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void startProjectById() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startProjectById(id);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startProjectByIndex(index);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void startProjectByName() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startProjectByName(name);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void finishProjectById() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishProjectById(id);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void finishProjectByIndex() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishProjectByIndex(index);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void finishProjectByName() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishProjectByName(name);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void changeProjectStatusById() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(ColorUtil.PURPLE + Arrays.toString(Status.values()) + ColorUtil.RESET);
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.changeProjectStatusById(id, status);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void changeProjectStatusByIndex() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(ColorUtil.PURPLE + Arrays.toString(Status.values()) + ColorUtil.RESET);
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.changeProjectStatusByIndex(index, status);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void changeProjectStatusByName() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(ColorUtil.PURPLE + Arrays.toString(Status.values()) + ColorUtil.RESET);
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.changeProjectStatusByName(name, status);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

}
