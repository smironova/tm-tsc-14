package com.tsc.smironova.tm.controller;

import com.tsc.smironova.tm.api.controller.ITaskController;
import com.tsc.smironova.tm.api.service.ITaskService;
import com.tsc.smironova.tm.enumerated.Sort;
import com.tsc.smironova.tm.enumerated.Status;
import com.tsc.smironova.tm.model.Task;
import com.tsc.smironova.tm.util.ColorUtil;
import com.tsc.smironova.tm.util.SystemOutUtil;
import com.tsc.smironova.tm.util.TerminalUtil;
import com.tsc.smironova.tm.util.ValidationUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTaskList() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(ColorUtil.PURPLE + Arrays.toString(Sort.values()) + ColorUtil.RESET);
        final String sort = TerminalUtil.nextLine();
        final List<Task> tasks;
        if (ValidationUtil.isEmpty(sort))
            tasks = taskService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(ColorUtil.PURPLE + sortType.getDisplayName() + ColorUtil.RESET);
            tasks = taskService.findAll(sortType.getComparator());
        }
        if (ValidationUtil.isEmpty(tasks)) {
            System.out.println(ColorUtil.RED + "No tasks! Add new task." + ColorUtil.RESET);
            SystemOutUtil.printFailMessage();
            return;
        }
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void showTask(final Task task) {
        if (task == null)
            return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus().getDisplayName());
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        showTask(task);
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        showTask(task);
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void showTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        showTask(task);
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void removeTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(id);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        showTask(task);
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeOneByIndex(index);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        showTask(task);
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void removeTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(name);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        showTask(task);
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdatedId = taskService.updateTaskById(id, name, description);
        if (taskUpdatedId == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdatedIndex = taskService.updateTaskByIndex(index, name, description);
        if (taskUpdatedIndex == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startTaskById(id);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.startTaskByIndex(index);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void startTaskByName() {
        System.out.println("[START TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startTaskByName(name);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void finishTaskById() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskById(id);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void finishTaskByIndex() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.finishTaskByIndex(index);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void finishTaskByName() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskByName(name);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(ColorUtil.PURPLE + Arrays.toString(Status.values()) + ColorUtil.RESET);
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeTaskStatusById(id, status);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(ColorUtil.PURPLE + Arrays.toString(Status.values()) + ColorUtil.RESET);
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeTaskStatusByIndex(index, status);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

    @Override
    public void changeTaskStatusByName() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(ColorUtil.PURPLE + Arrays.toString(Status.values()) + ColorUtil.RESET);
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeTaskStatusByName(name, status);
        if (task == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        SystemOutUtil.printOkMessage();
    }

}
