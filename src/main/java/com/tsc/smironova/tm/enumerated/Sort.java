package com.tsc.smironova.tm.enumerated;

import com.tsc.smironova.tm.comparator.ComparatorByCreated;
import com.tsc.smironova.tm.comparator.ComparatorByDateStart;
import com.tsc.smironova.tm.comparator.ComparatorByName;
import com.tsc.smironova.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    DATE_START("Sort by date start", ComparatorByDateStart.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private final String displayName;
    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.comparator = comparator;
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}

