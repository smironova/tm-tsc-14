package com.tsc.smironova.tm.service;

import com.tsc.smironova.tm.api.repository.ITaskRepository;
import com.tsc.smironova.tm.api.service.ITaskService;
import com.tsc.smironova.tm.enumerated.Status;
import com.tsc.smironova.tm.model.Task;
import com.tsc.smironova.tm.util.ValidationUtil;

import java.util.Comparator;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(Comparator<Task> taskComparator) {
        if (taskComparator == null)
            return null;
        return taskRepository.findAll(taskComparator);
    }

    @Override
    public Task add(final String name, final String description) {
        if (ValidationUtil.isEmpty(name) || ValidationUtil.isEmpty(description))
            return null;
        final Task task = new Task(name, description);
        add(task);
        return task;
    }

    @Override
    public void add(final Task task) {
        if (task == null)
            return;
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null)
            return;
        taskRepository.remove(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findOneById(String id) {
        if (ValidationUtil.isEmpty(id))
            return null;
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(Integer index) {
        if (ValidationUtil.checkIndex(index, taskRepository.size()))
            return null;
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task findOneByName(String name) {
        if (ValidationUtil.isEmpty(name))
            return null;
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task removeOneById(String id) {
        if (ValidationUtil.isEmpty(id))
            return null;
        return taskRepository.removeOneById(id);
    }

    @Override
    public Task removeOneByIndex(Integer index) {
        if (ValidationUtil.checkIndex(index, taskRepository.size()))
            return null;
        return taskRepository.removeOneByIndex(index);
    }

    @Override
    public Task removeOneByName(String name) {
        if (ValidationUtil.isEmpty(name))
            return null;
        return taskRepository.removeOneByName(name);
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description) {
        if (ValidationUtil.isEmpty(id) || ValidationUtil.isEmpty(name) || ValidationUtil.isEmpty(description))
            return null;
        final Task task = findOneById(id);
        if (task == null)
            return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description) {
        if (ValidationUtil.checkIndex(index, taskRepository.size()) || ValidationUtil.isEmpty(name) || ValidationUtil.isEmpty(description))
            return null;
        final Task task = findOneByIndex(index);
        if (task == null)
            return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(String id) {
        if (ValidationUtil.isEmpty(id))
            return null;
        final Task task = findOneById(id);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(Integer index) {
        if (ValidationUtil.checkIndex(index, taskRepository.size()))
            return null;
        final Task task = findOneByIndex(index);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(String name) {
        if (ValidationUtil.isEmpty(name))
            return null;
        final Task task = findOneByName(name);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskById(String id) {
        if (ValidationUtil.isEmpty(id))
            return null;
        final Task task = findOneById(id);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishTaskByIndex(Integer index) {
        if (ValidationUtil.checkIndex(index, taskRepository.size()))
            return null;
        final Task task = findOneByIndex(index);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishTaskByName(String name) {
        if (ValidationUtil.isEmpty(name))
            return null;
        final Task task = findOneByName(name);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeTaskStatusById(String id, Status status) {
        if (ValidationUtil.isEmpty(id) || status == null)
            return null;
        final Task task = findOneById(id);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;

    }

    @Override
    public Task changeTaskStatusByIndex(Integer index, Status status) {
        if (ValidationUtil.checkIndex(index, taskRepository.size()) || status == null)
            return null;
        final Task task = findOneByIndex(index);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;

    }

    @Override
    public Task changeTaskStatusByName(String name, Status status) {
        if (ValidationUtil.isEmpty(name) || status == null)
            return null;
        final Task task = findOneByName(name);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

}
