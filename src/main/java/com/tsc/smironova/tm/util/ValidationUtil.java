package com.tsc.smironova.tm.util;

import java.util.List;

public interface ValidationUtil {

    static boolean isEmpty(final String value) {
        return value == null || value.isEmpty();
    }

    static boolean isEmpty(final List value) {
        return value == null || value.isEmpty();
    }

    static boolean checkIndex(final Integer index, final int size) {
        return index == null || index < 0 || index > size - 1;
    }

}
